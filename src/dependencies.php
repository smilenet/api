<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// services

require 'services/project-service.php';

$container['projectService'] = function ($container) {
    $service = new ProjectService();
    return $service;
};

require 'services/user-service.php';

$container['userService'] = function ($container) {
    $service = new UserService();
    return $service;
};

require 'services/auth-service.php';

$container['authService'] = function ($container) {
    $service = new AuthService();
    return $service;
};