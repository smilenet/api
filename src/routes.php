<?php

$authMiddleware = function ($request, $response, $next) {
    if($request->hasHeader('HTTP_AUTH')){
        try{
            $this->authService->Check($request->getHeaderLine('HTTP_AUTH'));  
            $response = $next($request, $response);
            return $response;
        }catch(Exception $e){
            return $response->withStatus(401);
        }        
    }else{
        return $response->withStatus(401);
    }   
};

$app->post('/api/admin/auth', function ($request, $response, $args) {
    $json = $request->getParsedBody();

    if(!isset($json['user']) || !isset($json['pass'])) {
        return $response->withStatus(400);
    }
    $user = $this->userService->userAuth($json['user'], $json['pass']);

    if($user != false){
        $token = $this->authService->SignIn([
            'id' => $user['id'],
            'name' => $user['username']
        ]);  
        return $response->withJson(array("token" => $token));      
    }else{
        return $response->withStatus(400);
    }
});

$app->post('/api/admin/logout', function ($request, $response, $args) {
    $json = $request->getParsedBody();
    $response = $response->withStatus(200);

    $session = session_id();
    if(empty($session)) session_start();

    return $response;
});

$app->get('/api/public/projects', function ($request, $response, $args) {
    $params = $request->getQueryParams();
    if(!isset($params['page']) || !isset($params['size'])) {
        return $response->withStatus(400);
    }
    return $response->withJson($this->projectService->findAllPublic($params['page'], $params['size']));
});

$app->get('/api/public/projects/{id}', function ($request, $response, $args) {
    $idProject = $args['id'];
    return $response->withJson($this->projectService->findOnePublic($idProject));
});

$app->group('/api', function () use ($app) {

    // PROJECTS

    $app->get('/projects', function ($request, $response, $args) {
        $params = $request->getQueryParams();
        if(!isset($params['page']) || !isset($params['size'])) {
            return $response->withStatus(400);
        }
        return $response->withJson($this->projectService->findAll($params['page'], $params['size']));
    });  
    
    $app->get('/projects/{id}', function ($request, $response, $args) {
    	$idProject = $args['id'];
    	return $response->withJson($this->projectService->findOne($idProject));
    });

    $app->post('/projects', function ($request, $response, $args) {
        $json = $request->getParsedBody();
        $id = $this->projectService->save($json);
        return $response->withStatus(201)->withJson($id);
    });

    $app->put('/projects/{id}', function ($request, $response, $args) {
        $idProject = $args['id'];
        $json = $request->getParsedBody();
        $this->projectService->update($idProject, $json);
        return $response->withStatus(202);
    });    

    $app->delete('/projects/{id}', function ($request, $response, $args) {
        $idProject = $args['id'];
        $this->projectService->delete($idProject);
        return $response->withStatus(202);
    });

    $app->get('/projects/{id}/gallery', function ($request, $response, $args) {
        $idProject = $args['id'];
        return $response->withJson($this->projectService->findImages($idProject));
    });

    $app->post('/projects/{id}/gallery', function ($request, $response, $args) {
        $files = $request->getUploadedFiles();
        try{
            foreach ($files as $file){
                $this->projectService->saveImage($args['id'], $file);                          
            }
        }catch(Exception $e){
            return $response->withStatus(400)->withJson(array("message" => $e->getMessage()));
        }

        return $response->withStatus(201);
    });

    $app->delete('/projects/{id}/gallery/{image}', function ($request, $response, $args) {
        $idProject = $args['id'];
        $idImage = $args['image'];
        $this->projectService->deleteImage($idProject, $idImage);
        return $response->withStatus(202);
    });

    // USERS

    $app->get('/users', function ($request, $response, $args) {
        $params = $request->getQueryParams();
        if(!isset($params['page']) || !isset($params['size'])) {
            return $response->withStatus(400);
        }
        return $response->withJson($this->userService->findAll($params['page'], $params['size']));
    });

    $app->get('/users/{id}', function ($request, $response, $args) {
        $idUser = $args['id'];
        return $response->withJson($this->userService->findOne($idUser));
    });

    $app->post('/users', function ($request, $response, $args) {
        $json = $request->getParsedBody();
        $id = $this->userService->save($json);
        return $response->withStatus(201)->withJson($id);
    });

    $app->put('/users/{id}', function ($request, $response, $args) {
        $idUser = $args['id'];
        $json = $request->getParsedBody();
        $this->userService->update($idUser, $json);
        return $response->withStatus(202);
    });

    $app->delete('/users/{id}', function ($request, $response, $args) {
        $idUser = $args['id'];
        $this->userService->delete($idUser);
        return $response->withStatus(202);
    });

})->add($authMiddleware);