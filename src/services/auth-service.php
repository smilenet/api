<?php
use Firebase\JWT\JWT;

class AuthService
{
    private $secret_key = 'Sm@1Ln3t';
    private $encrypt = array('HS256');
    private $aud = null;
    private $minutes_to_expire = 60;
    
    public function SignIn($data)
    {
        $time = time();
        
        $token = array(
            'exp' => $time + (60 * $this->minutes_to_expire),
            'aud' => $this->Aud(),
            'data' => $data
        );

        return JWT::encode($token, $this->secret_key);
    }
    
    public function Check($token)
    {
        if(empty($token))
        {
            throw new Exception("Invalid token supplied.");
        }
        
        $decode = JWT::decode($token, $this->secret_key, $this->encrypt);
        
        if($decode->aud !== $this->Aud())
        {
            throw new Exception("Invalid user logged in.");
        }
    }
    
    public function GetData($token)
    {
        return JWT::decode(
            $token,
            $this->$secret_key,
            $this->$encrypt
        )->data;
    }
    
    private function Aud()
    {
        $aud = '';
        
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $aud = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $aud = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $aud = $_SERVER['REMOTE_ADDR'];
        }
        
        $aud .= @$_SERVER['HTTP_USER_AGENT'];
        $aud .= gethostname();
        
        return sha1($aud);
    }
}