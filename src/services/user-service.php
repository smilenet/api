<?php

class UserService
{
    public function findOne($id) {
        $user = Model::factory('User')->find_one($id);
        return $user->as_array();
    }

    public function userAuth($user, $pass) {
        $user = Model::factory('User')->where("username", $user)->where("password", $pass)->find_one();
        if($user != false){
            return $user->as_array();
        }else{
            return false;
        }
    }

    public function findAll($page, $size) {
        $rows = Model::factory('User')->limit($size)->offset($page * $size)->order_by_asc('name')->find_many();
        $total = Model::factory('User')->count();
        $results = array();
        foreach ($rows as $valor) {
            array_push($results, $valor->as_array());
        }
        return array("users" => $results, "total" => $total);
    }

    public function save($user) {
        $model = Model::factory('User')->create();
        $model->name = $user['name'];
        $model->username = $user['username'];
        $model->email = $user['email'];
        $model->password = $user['password'];
        $model->save();
        return $model->as_array('id');
    }

    public function update($id, $user) {
        $model = Model::factory('User')->find_one($id);
        $model->name = $user['name'];
        $model->username = $user['username'];
        $model->email = $user['email'];
        $model->password = $user['password'];
        $model->save();
    }

    public function delete($id) {
        $model = Model::factory('User')->find_one($id);
        $model->delete();
    }
}