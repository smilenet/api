<?php

class ProjectService
{
    private $imageProjectsFolder = 'public/uploads/projects/';

    public function findOne($id) {
        $project = Model::factory('Project')->find_one($id);
        return $project->as_array();
    }

    public function findImages($idProject) {
        $projectImages = Model::factory('ProjectImage')->where("project_id", $idProject)->find_many();
        $results = array();
        foreach ($projectImages as $val) {
            array_push($results, $val->as_array());
        }
        return $results;
    }

    public function findAll($page, $size) {
        $rows = Model::factory('Project')->limit($size)->offset($page * $size)->order_by_desc('start')->find_many();
        $total = Model::factory('Project')->count();
        $results = array();
        foreach ($rows as $val) {
            array_push($results, $val->as_array('id', 'name', 'person', 'start', 'end'));
        }
        return array("projects" => $results, "total" => $total);
    }

    public function findOnePublic($idProject) {
        $project = Model::factory('Project')->find_one($idProject);

        $projectImages = $project->images()->find_many();
        $imagesArray = array();
        foreach ($projectImages as $imgval) {
            array_push($imagesArray, $imgval->as_array());
        }
        $project->images = $imagesArray;
        return $project->as_array();
    }

    public function findAllPublic($page, $size) {
        $now = date("Y-m-d");
        $rows = Model::factory('Project')->where_gte('end', $now)->where_lte('start', $now)->limit($size)->offset($page * $size)->order_by_asc('end')->find_many();
        $total = Model::factory('Project')->where_gte('end', $now)->where_lte('start', $now)->count();
        $results = array();
        foreach ($rows as $val) {
            $projectImages = $val->images()->find_many();
            $imagesArray = array();
            foreach ($projectImages as $imgval) {
                array_push($imagesArray, $imgval->as_array());
            }
            $val->images = $imagesArray;
            array_push($results, $val->as_array());
            
        }
        return array("projects" => $results, "total" => $total);
    }

    public function save($project) {
        $model = Model::factory('Project')->create();
        $model->name = $project['name'];
        $model->person = $project['person'];
        $model->abstract_person = $project['abstract_person'];
        $model->abstract_project = $project['abstract_project'];
        $model->necessity = $project['necessity'];
        $model->vision = $project['vision'];
        $model->start = date('Y-m-d', strtotime($project['start']));
        $model->end = date('Y-m-d', strtotime($project['end']));
        $model->amount = $project['amount'];
        $model->video = $project['video'];
        $model->save();
        return $model->as_array('id');
    }

    public function update($id, $project) {
        $model = Model::factory('Project')->find_one($id);
        $model->name = $project['name'];
        $model->person = $project['person'];
        $model->abstract_person = $project['abstract_person'];
        $model->abstract_project = $project['abstract_project'];
        $model->necessity = $project['necessity'];
        $model->vision = $project['vision'];
        $model->start = date('Y-m-d', strtotime($project['start']));
        $model->end = date('Y-m-d', strtotime($project['end']));
        $model->amount = $project['amount'];
        $model->video = $project['video'];
        $model->save();
    }

    public function delete($id) {
        $images = Model::factory('ProjectImage')->where("project_id", $id)->find_many();
        foreach ($images as $val) {
            unlink($this->imageProjectsFolder.$val->name.'.'.$val->ext);
            $val->delete();
        }
        $model = Model::factory('Project')->find_one($id);
        $model->delete();
    }

    public function saveImage($idProject, $file) {
        $name = uniqid($idProject.'-');       
        $ext = substr($file->getClientFilename(), strripos($file->getClientFilename(),'.') + 1, strlen($file->getClientFilename()) - 1);
        $file->moveTo($this->imageProjectsFolder.$name.'.'.$ext); 

        $model = Model::factory('ProjectImage')->create();
        $model->project_id = $idProject; 
        $model->name = $name; 
        $model->ext = $ext; 
        $model->save();
    }

    public function deleteImage($idProject, $idImage) {        
        $model = Model::factory('ProjectImage')->find_one($idImage);
        $image = $model->as_array();

        if($image['project_id'] == $idProject){
            unlink($this->imageProjectsFolder.$image['name'].'.'.$image['ext']);
            $model->delete();
        }else{
            return new Exception('Imagen no corresponde al proyecto');
        }        
    }
}