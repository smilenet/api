<?php

ORM::configure('mysql:host=localhost;dbname=smilenet_wp1');
ORM::configure('username', 'smilenet_wp1');
ORM::configure('password', 'K.oSME&S0BIA#E[zQz.78(#7');

/**
 * @property int $id
 * @property string $name
 * @property string $person
 * @property string $abstract_person
 * @property string $abstract_project
 * @property string $necessity
 * @property string $vision
 * @property date $start
 * @property date $end
 * @property integer $amount
 * @property string $video
 */
class Project extends Model {
    public function images() {
        return $this->has_many('ProjectImage');
    }
}

/**
 * @property int $id
 * @property number $id_project
 * @property string $name
 * @property string $ext
 */
class ProjectImage extends Model {
}

/**
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $password
 */
class User extends Model {
}